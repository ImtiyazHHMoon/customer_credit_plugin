﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public interface ICustomerCreditDetailsFactory
    {
        Task<CustomerCreditDetailsSearchModel> PrepareCustomerCreditDetailsSearchModel(CustomerCreditDetailsSearchModel searchModel);
        Task<CustomerCreditDetailsListModel> PrepareCustomerCreditDetailsListModel(CustomerCreditDetailsSearchModel searchModel);
        Task<CustomerCreditDetailsListModel> PrepareCustomerCreditDetailsListModelWithQuery(CustomerCreditDetailsSearchModel searchModel);

    }
}

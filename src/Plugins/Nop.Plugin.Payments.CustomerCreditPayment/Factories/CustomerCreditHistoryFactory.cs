﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public class CustomerCreditHistoryFactory : ICustomerCreditHistoryFactory
    {
        private readonly ICustomerCreditHistoryService _customerCreditHistoryService;

        public CustomerCreditHistoryFactory(ICustomerCreditHistoryService customerCreditHistoryService)
        {
            _customerCreditHistoryService = customerCreditHistoryService;
        }
        public async Task<CustomerCreditHistoryListModel> PrepareCustomerCreditHistoryListModel(CustomerCreditHistorySearchModel searchModel)
        {
            var creditHistoryList = await _customerCreditHistoryService.GetCustomerAllCreditHistoryAsync(searchModel.CustomerId);

            var model = new CustomerCreditHistoryListModel().PrepareToGrid(searchModel, creditHistoryList, () => {

                return creditHistoryList.Select(creditHistory =>
                {
                    return new CustomerCreditHistoryModel
                    {
                        Id = creditHistory.Id,
                        ActivityType = creditHistory.ActivityType,
                        NopCustomerId = creditHistory.NopCustomerId,
                        ActivityDateUtc = creditHistory.ActivityDateUtc,
                        NewValueOfCreditUsed = creditHistory.NewValueOfCreditUsed,
                        PrevValueOfCreditUsed = creditHistory.PrevValueOfCreditUsed,
                        ActivityTypeId = creditHistory.ActivityTypeId,
                        CreditUsedAdjustment = creditHistory.CreditUsedAdjustment,
                        Description = creditHistory.Description,
                        UsedCurrency = creditHistory.UsedCurrency
                    };
                });

            });

            return model;
        }

        public Task<CustomerCreditHistorySearchModel> PrepareCustomerCreditHistorySearchModel(CustomerCreditHistorySearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return Task.FromResult(searchModel);
        }
    }
}

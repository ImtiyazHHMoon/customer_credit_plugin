﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public class CustomerCreditPaymentFactory : ICustomerCreditPaymentFactory
    {
        private readonly ICustomerCreditPaymentService _customerCreditPaymentService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreService _storeService;

        public CustomerCreditPaymentFactory(ICustomerCreditPaymentService customerCreditPaymentService, ILocalizationService localizationService
            , IStoreService storeService )
        {
            _customerCreditPaymentService = customerCreditPaymentService;
            _localizationService = localizationService;
            _storeService = storeService;
        }
        public Task<CustomerInvoicePaymentSearchModel> PreapreCustomerInvoicePaymentSearchModel(CustomerInvoicePaymentSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return Task.FromResult(searchModel);
        }

        public async Task<CustomCreditPaymentListModel> PrepareCustomCreditPaymentListModel(CustomerInvoicePaymentSearchModel searchModel)
        {
            var invoiceList = await _customerCreditPaymentService.GetCustomerInvoicePaymentList(searchModel.CustomerId);

            var model =  new CustomCreditPaymentListModel().PrepareToGrid(searchModel, invoiceList, () =>
             {
                 return invoiceList.Select(creditPayment =>
                 {
                     return new CustomCreditPaymentModel {
                        Amount = creditPayment.Amount,
                        NopCustomerId = creditPayment.NopCustomerId,
                        PaymentDateUtc = creditPayment.PaymentDateUtc,
                        InvoiceReference = creditPayment.InvoiceReference,
                        CreatedById = creditPayment.CreatedById,
                        EditedById = creditPayment.EditedById,
                        Id = creditPayment.Id

                    };
                 });
             });
            return model;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Customers;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public class CustomerCreditDetailsFactory : ICustomerCreditDetailsFactory
    {
        private readonly ICustomerService _customerService;
        private readonly CustomerSettings _customerSettings;
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;

        public CustomerCreditDetailsFactory(ICustomerService customerService, ICustomerCreditDetailsService customerCreditDetailsService,
            CustomerSettings customerSettings)
        {
            _customerService = customerService;
            _customerCreditDetailsService = customerCreditDetailsService;
            _customerSettings = customerSettings;
        }
        public async Task<CustomerCreditDetailsListModel> PrepareCustomerCreditDetailsListModel(CustomerCreditDetailsSearchModel searchModel)
        {
            var creditCustomersList = await _customerCreditDetailsService.GetAllCreditedCustomer();                

            var ccIds = creditCustomersList.Select(c => c.NopCustomerId);
            var customers = await _customerService.GetCustomersByIdsAsync(ccIds.ToArray());

            var model = new CustomerCreditDetailsListModel().PrepareToGridAsync(searchModel, creditCustomersList, () =>
            {
                return creditCustomersList.SelectAwait(async ccmodel =>
                {
                    var customer = customers.FirstOrDefault(c => c.Id == ccmodel.NopCustomerId);
                    if (customer != null)
                    {
                        return new CustomerCreditDetailsModel
                        {
                            NopCustomerId = customer.Id,
                            Email = customer.Email,
                            Id = ccmodel.Id,
                            FullName = await _customerService.GetCustomerFullNameAsync(customer),
                            AvailableCredit = ccmodel.AvailableCredit,
                            CreditLimt = ccmodel.CreditLimt,
                            CreditUsed = ccmodel.CreditUsed,
                            IsCreditPaymentActive = ccmodel.IsCreditPaymentActive,
                            AllowOverspend = ccmodel.AllowOverspend,
                        };
                    }else
                        return null;
                });
            });
            return await model;

        }

        public async Task<CustomerCreditDetailsListModel> PrepareCustomerCreditDetailsListModelWithQuery(CustomerCreditDetailsSearchModel searchModel)
        {
            var  creditCustomersList = await _customerCreditDetailsService.GetAllCreditedCustomerWithQuery(searchModel.SelectedCustomerRoleIds.ToArray(),searchModel.Email,
                searchModel.FirstName,searchModel.LastName);

            var ccIds = creditCustomersList.Select(c => c.NopCustomerId);
            var customers = await _customerService.GetCustomersByIdsAsync(ccIds.ToArray());

            var model = new CustomerCreditDetailsListModel().PrepareToGridAsync(searchModel, creditCustomersList, () =>
            {
                return creditCustomersList.SelectAwait(async ccmodel =>
                {
                    var customer = customers.FirstOrDefault(c => c.Id == ccmodel.NopCustomerId);
                    if (customer != null)
                    {
                        return new CustomerCreditDetailsModel
                        {
                            NopCustomerId = customer.Id,
                            Email = customer.Email,
                            Id = ccmodel.Id,
                            FullName = await _customerService.GetCustomerFullNameAsync(customer),
                            AvailableCredit = ccmodel.AvailableCredit,
                            CreditLimt = ccmodel.CreditLimt,
                            CreditUsed = ccmodel.CreditUsed,
                            IsCreditPaymentActive = ccmodel.IsCreditPaymentActive,
                            AllowOverspend = ccmodel.AllowOverspend,
                            CustomerRole = string.Join(", ",
                                (await _customerService.GetCustomerRolesAsync(customer)).Select(role => role.Name))
                        };
                    }
                    else
                        return null;
                });
            });
            return await model;
        }



        public async Task<CustomerCreditDetailsSearchModel> PrepareCustomerCreditDetailsSearchModel(CustomerCreditDetailsSearchModel searchModel)
        {
            searchModel.FirstNameEnabled = _customerSettings.FirstNameEnabled;
            searchModel.LastNameEnabled = _customerSettings.LastNameEnabled;

            var registeredRole = await _customerService.GetCustomerRoleBySystemNameAsync(NopCustomerDefaults.RegisteredRoleName);
            if (registeredRole != null)
                searchModel.SelectedCustomerRoleIds.Add(registeredRole.Id);

            //prepare available customer roles
            var availableRoles = await _customerService.GetAllCustomerRolesAsync(showHidden: true);
            searchModel.AvailableCustomerRoles = availableRoles.Select(role => new SelectListItem
            {
                Text = role.Name,
                Value = role.Id.ToString(),
                Selected = searchModel.SelectedCustomerRoleIds.Contains(role.Id)
            }).ToList();

            searchModel.SetGridPageSize();

            return searchModel;
        }
    }
}

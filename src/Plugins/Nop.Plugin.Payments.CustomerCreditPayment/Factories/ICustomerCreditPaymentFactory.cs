﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public interface ICustomerCreditPaymentFactory
    {
        Task<CustomerInvoicePaymentSearchModel> PreapreCustomerInvoicePaymentSearchModel(CustomerInvoicePaymentSearchModel model);
        Task<CustomCreditPaymentListModel> PrepareCustomCreditPaymentListModel (CustomerInvoicePaymentSearchModel model);
    }
}

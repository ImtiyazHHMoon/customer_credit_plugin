﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Factories
{
    public interface ICustomerCreditHistoryFactory
    {
        Task<CustomerCreditHistorySearchModel> PrepareCustomerCreditHistorySearchModel(CustomerCreditHistorySearchModel searchModel);
        Task<CustomerCreditHistoryListModel> PrepareCustomerCreditHistoryListModel(CustomerCreditHistorySearchModel searchModel);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Payments.CustomerCreditPayment
{
    public class CustomerCreditPaymentProcessor : BasePlugin, IPaymentMethod,IWidgetPlugin, IAdminMenuPlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;
        private readonly ICustomerCreditUsedOrderService _customerCreditUsedOrderService;
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;
        private readonly ICustomerCreditHistoryService _customerCreditHistory ;
        private readonly IStoreContext _storeContext;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IWorkContext _workContext;

        public CustomerCreditPaymentProcessor(ILocalizationService localizationService,
            IShoppingCartService shoppingCartService, IWebHelper webHelper,
            ISettingService settingService, ICustomerCreditUsedOrderService customerCreditUsedOrderService,
            ICustomerCreditHistoryService customerCreditHistory, ICustomerCreditDetailsService customerCreditDetailsService,
            IStoreContext storeContext, ICurrencyService currencyService, CurrencySettings currencySettings, IWorkContext workContext)
        {
            _localizationService = localizationService;
            _shoppingCartService = shoppingCartService;
            _webHelper = webHelper;
            _settingService = settingService; 
            _customerCreditUsedOrderService = customerCreditUsedOrderService;
            _customerCreditHistory = customerCreditHistory;
            _customerCreditDetailsService = customerCreditDetailsService;
            _storeContext = storeContext;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
            _workContext = workContext;
        }
        public bool SupportCapture => false;

        public bool SupportPartiallyRefund => false;

        public bool SupportRefund => false;

        public bool SupportVoid => false;

        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        public PaymentMethodType PaymentMethodType => PaymentMethodType.Standard;

        public bool SkipPaymentInfo
        {
            get 
            {
                var storeScope = _storeContext.GetCurrentStore().Id;
                var cpSettings = _settingService.LoadSettingAsync<CustomerCreditPaymentSettings>(storeScope).Result;

                return cpSettings.SkipPaymentInfo;
            }
        }

        public bool HideInWidgetList => false;

        public Task<CancelRecurringPaymentResult> CancelRecurringPaymentAsync(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            return Task.FromResult(new CancelRecurringPaymentResult { Errors = new[] { "Recurring payment not supported" } });
        }

        public Task<bool> CanRePostProcessPaymentAsync(Order order)
        {
            return Task.FromResult(false);
        }

        public Task<CapturePaymentResult> CaptureAsync(CapturePaymentRequest capturePaymentRequest)
        {
            return Task.FromResult(new CapturePaymentResult { Errors = new[] { "Capturing not supported" } });
        }

        public Task<decimal> GetAdditionalHandlingFeeAsync(IList<ShoppingCartItem> cart)
        {
            return Task.FromResult<decimal>(0);
        }

        public async Task<ProcessPaymentRequest> GetPaymentInfoAsync(IFormCollection form)
        {
            var model = new ProcessPaymentRequest();
            model.CustomValues.Add("OrderReferrence", form["OrderReference"]);
            return await Task.FromResult(model);
        }

        public async Task<string> GetPaymentMethodDescriptionAsync()
        {
            var storeScope = await _storeContext.GetCurrentStoreAsync();
            var cpSettings = await _settingService.LoadSettingAsync<CustomerCreditPaymentSettings>(storeScope.Id);

            return cpSettings.DescriptionText;
        }

        public string GetPublicViewComponentName()
        {
            return "CustomerCreditPayment";
        }

        public async Task<bool> HidePaymentMethodAsync(IList<ShoppingCartItem> cart)
        {

            if (cart.Count > 0)
            {
                var cartOne = cart.FirstOrDefault();

                var cusCreditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(cartOne.CustomerId);
                if (cusCreditDetails == null || !cusCreditDetails.IsCreditPaymentActive)
                    return true;
            }
            return !await _shoppingCartService.ShoppingCartRequiresShippingAsync(cart);
        }

        public  Task PostProcessPaymentAsync(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            return Task.CompletedTask;
        }

        public async Task<ProcessPaymentResult> ProcessPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            if (processPaymentRequest != null)
            {
                var currCurrency = await _workContext.GetWorkingCurrencyAsync();
                //var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
                var mode = new CustomerCreditUsedOrder
                {
                    NopCustomerId = processPaymentRequest.CustomerId,
                    OrderGuid = processPaymentRequest.OrderGuid,
                    OrderTotal = processPaymentRequest.OrderTotal,
                    OrderReference = processPaymentRequest.CustomValues["OrderReferrence"].ToString(),
                    UsedCurrency = currCurrency.CurrencyCode
                };

                var creditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(processPaymentRequest.CustomerId);
                if(!creditDetails.IsCreditPaymentActive)
                {
                    var errResult = new ProcessPaymentResult();
                    errResult.AddError("You are not allowed to pay using credit");
                    return errResult;
                }

                
                //var primaryCurrencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;
                
                
                creditDetails.CreditUsed += mode.OrderTotal;
                creditDetails.AvailableCredit = creditDetails.AvailableCredit - mode.OrderTotal;

                if (creditDetails.AvailableCredit < 0)
                {
                    if (!creditDetails.AllowOverspend)
                    {
                        var errResult = new ProcessPaymentResult();
                        errResult.AddError("Credit is not sufficient");
                        return errResult;
                    }  
                }                

                await _customerCreditUsedOrderService.InsertCreditUsedOrderAsync(mode);

                await _customerCreditDetailsService.UpdateCreditDetailsAsync(creditDetails);

                var creditHistoryPrev = await _customerCreditHistory.GetCustomerCreditHistoryAsync(processPaymentRequest.CustomerId);
                var creditHistory = new CustomerCreditHistory
                {
                    NopCustomerId = processPaymentRequest.CustomerId,
                    ActivityDateUtc = DateTime.UtcNow,
                    NewValueOfCreditUsed = creditDetails.CreditUsed,
                    ActivityTypeId = (int)CreditActivityType.OrderPlaced,
                    PrevValueOfCreditUsed = creditHistoryPrev.NewValueOfCreditUsed,
                    UsedCurrency = currCurrency.CurrencyCode,
                    Description = "Order payment paid using credit"
                };

                await _customerCreditHistory.InsertCustomerCreditPaymentAsync(creditHistory);
            }
            return new ProcessPaymentResult { NewPaymentStatus = PaymentStatus.Paid };
        }

        public Task<ProcessPaymentResult> ProcessRecurringPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            return Task.FromResult(new ProcessPaymentResult { Errors = new[] { "Recurring payment not supported" } });
        }

        public Task<RefundPaymentResult> RefundAsync(RefundPaymentRequest refundPaymentRequest)
        {
            return Task.FromResult(new RefundPaymentResult { Errors = new[] { "Refund method not supported" } });
        }

        public Task<IList<string>> ValidatePaymentFormAsync(IFormCollection form)
        {
            return Task.FromResult<IList<string>>(new List<string>());
        }

        public Task<VoidPaymentResult> VoidAsync(VoidPaymentRequest voidPaymentRequest)
        {
            return Task.FromResult(new VoidPaymentResult { Errors = new[] { "Void method not supported" } });
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CustomerCreditPayment/Configure";
        }

        public override async Task InstallAsync()
        {
            var settings = new CustomerCreditPaymentSettings
            {
                DescriptionText = "Credit payment",
                SkipPaymentInfo = false,
                MinimumCredit = 0
            };

            await _settingService.SaveSettingAsync(settings);
            //locales
            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Payments.CustomerCreditPayment.PaymentMethodDescription"] = "Description",
                ["Plugins.Payments.CustomerCreditPayment.Fields.IsCreditPaymentActive"] = "Credit payment activate",
                ["Plugins.Payments.CustomerCreditPayment.Fields.CreditLimt"] = "Customer credit limit",
                ["Plugins.Payments.CustomerCreditPayment.Fields.CreditUsed"] = "Customer credit used",
                ["Plugins.Payments.CustomerCreditPayment.Fields.AvailableCredit"] = "Available credit",
                ["Plugins.Payments.CustomerCreditPayment.Fields.AllowOverspend"] = "Allow overspend",
                ["Plugins.Payments.CustomerCreditPayment.Fields.PrevValueOfCreditUsed"] = "Previously used credit",
                ["Plugins.Payments.CustomerCreditPayment.Fields.CreditUsedAdjustment"] = "Adjust used credit",
                ["Plugins.Payments.CustomerCreditPayment.Fields.NewValueOfCreditUsed"] = "New credit used",
                ["Plugins.Payments.CustomerCreditPayment.Fields.ActivityDateUtc"] = "Activity date",
                ["Plugins.Payments.CustomerCreditPayment.Fields.ActivityType"] = "Activity type",
                ["Plugins.Payments.CustomerCreditPayment.Fields.InvoiceReference"] = "Invoice referrence",
                ["Plugins.Payments.CustomerCreditPayment.Fields.PaymentDateUtc"] = "Payment date",
                ["Plugins.Payments.CustomerCreditPayment.Fields.CreatedById"] = "Created by",
                ["Plugins.Payments.CustomerCreditPayment.Fields.EditedById"] = "Edited by",
                ["Plugins.Payments.CustomerCreditPayment.Fields.Amount"] = "Amount",
                ["Plugins.Payments.CustomerCreditPayment.Fields.Description"] = "Description",
                ["Plugins.Payments.CustomerCreditPayment.Fields.Email"] = "Email",
                ["Plugins.Payments.CustomerCreditPayment.Fields.CustomerRole"] = "CustomerRole",
                ["Plugins.Payments.CustomerCreditPayment.Fields.UsedCurrency"] = "Currency used in payment",
                ["Plugins.Payments.CustomerCreditPayment.List.FirstName"] = "First nmae",
                ["Plugins.Payments.CustomerCreditPayment.List.LastName"] = "Last nmae",
                ["Plugins.Payments.CustomerCreditPayment.List.Email"] = "Email",
                ["Plugins.Payments.CustomerCreditPayment.List.CustomerRoles"] = "CustomerRoles",
                ["Plugins.Payments.CustomerCreditPayment.CreditDetailsTitle"] = "Customer credit details",
                ["Plugins.Payments.CustomerCreditPayment.CreditDetailsListTitle"] = "Credited customers",
                ["Plugins.Payments.CustomerCreditPayment.AvailableCredit"] = "",
                ["Plugins.Payments.CustomerCreditPayment.InvoiceCreateButton"] = "Add invoice payment",
                ["Plugins.Payments.CustomerCreditPayment.InvoicePayment.AddNew"] = "Add new invoice",
                ["Plugins.Payments.CustomerCreditPayment.Fields.FullName"] = "Customer fullname",
                ["Plugins.Payments.CustomerCreditPayment.InvoicePaymentTitle"] = "Invoice payment",
                ["Plugins.Payments.CustomerCreditPayment.CreditHistoryTitle"] = "Credit history",
                ["Plugins.Payments.CustomerCreditPayment.SkipPaymentInfo"] = "Skip payment info",
                ["Plugins.Payments.CustomerCreditPayment.PublicPageTitle"] = "Credit details",
                ["Plugins.Payments.CustomerCreditPayment.NoCreditDetails"] = "Credit details not available",
                ["Plugins.Payments.CustomerCreditPayment.MinimumCredit"] = "Minimum credit",
            });
            ;
            await base.InstallAsync();
        }

        public override async Task UninstallAsync()
        {
            await _settingService.DeleteSettingAsync<CustomerCreditPaymentSettings>();
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Payments.CustomerCreditPayment");
            await base.UninstallAsync();
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { AdminWidgetZones.CustomerDetailsBlock, "credit_details_bottom_block",
            "credit_invoice_payment_bottom_block",PublicWidgetZones.AccountNavigationAfter});
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            if (widgetZone == "admin_customer_details_block")
                return "CreditPaymentCustomerDetails";
            else if (widgetZone == "credit_details_bottom_block")
                return "CustomerInvoicePaymentList";
            else if (widgetZone == "credit_invoice_payment_bottom_block")
                return "CustomerCreditHistory";
            else if (widgetZone == "account_navigation_after")
                return "CustomerCreditDetailsNavigationButton";
            return widgetZone;
        }

        public Task ManageSiteMapAsync(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                SystemName = "Payments.CustomerCreditPayment",
                Title = "Credit enabled customers",
                //ControllerName = @"Admin\/\CustomerCreditPayment",
                Url = "~/Admin/CustomerCreditPayment/AllCreditCustomerList",
                //ActionName = "AllCreditCustomerList",
                IconClass = "nav-icon far fa-dot-circle",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "area", null } },
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Customers");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);

            return Task.CompletedTask;
        }
    }
}

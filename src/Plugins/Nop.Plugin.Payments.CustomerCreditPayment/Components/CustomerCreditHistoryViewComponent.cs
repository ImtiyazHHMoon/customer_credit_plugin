﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Payments.CustomerCreditPayment.Factories;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Components
{
    public class CustomerCreditHistoryViewComponent: NopViewComponent
    {
        private readonly ICustomerCreditHistoryFactory _customerCreditHistoryFactory;
        public CustomerCreditHistoryViewComponent(ICustomerCreditHistoryFactory customerCreditHistoryFactory)
        {
            _customerCreditHistoryFactory = customerCreditHistoryFactory;
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            if (!(additionalData is CustomerInvoicePaymentSearchModel customer))
                return Content("");

            if (customer.CustomerId == 0)
                return Content("");
            ///
            var creditHistory = await _customerCreditHistoryFactory.PrepareCustomerCreditHistorySearchModel(new Models.CustomerCreditHistorySearchModel
            {
                CustomerId = customer.CustomerId,
            });

            return View("~/Plugins/Payments.CustomerCreditPayment/Views/CustomerCreditHistoryList.cshtml", creditHistory);
        }
    }
}

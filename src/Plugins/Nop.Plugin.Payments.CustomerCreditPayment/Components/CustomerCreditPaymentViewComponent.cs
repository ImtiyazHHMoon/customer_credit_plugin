﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Components
{
    [ViewComponent]
    public class CustomerCreditPaymentViewComponent : NopViewComponent
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private ICustomerCreditDetailsService _customerCreditDetailsService;
        private ICurrencyService _currencyService;

        public CustomerCreditPaymentViewComponent(ILocalizationService localizationService,
            ISettingService settingService,
            IStoreContext storeContext,
            IWorkContext workContext,
            ICustomerCreditDetailsService customerCreditDetailsService,
            ICurrencyService currencyService)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _storeContext = storeContext;
            _workContext = workContext;
            _customerCreditDetailsService = customerCreditDetailsService;
            _currencyService = currencyService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var currentStore = await _storeContext.GetCurrentStoreAsync();
            var currentLanguage = await _workContext.GetWorkingLanguageAsync();
            var creditPaymentSettings = await _settingService.LoadSettingAsync<CustomerCreditPaymentSettings>(currentStore.Id);

            var customer = await _workContext.GetCurrentCustomerAsync();
            var customerCreditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(customer.Id);

            //if (!customerCreditDetails.IsCreditPaymentActive)
            //    return Content("");
            var currentCurrencey = await _workContext.GetWorkingCurrencyAsync();
            //var exRate = await _currencyService.GetCurrencyLiveRatesAsync(currentCurrencey.CurrencyCode);
            var currency = await _currencyService.ConvertFromPrimaryStoreCurrencyAsync(customerCreditDetails.AvailableCredit, currentCurrencey);
            var currencyCode = currentCurrencey.CurrencyCode;
            
            var model = new PaymentInfoModel
            {
                DescriptionText = await _localizationService.GetLocalizedSettingAsync(creditPaymentSettings, x => x.DescriptionText, currentLanguage.Id, 0),
                AvailableCredit = currency.ToString("0.00"),
                PrimaryStoreCurrencyCode = currencyCode,
            };
            if (creditPaymentSettings.MinimumCredit >= customerCreditDetails.AvailableCredit)
            {
                model.HaveWarningMsg = true;
                model.WarningMsg = "You have lower credit";
            }

            return View("~/Plugins/Payments.CustomerCreditPayment/Views/PaymentInfo.cshtml", model);
        }
    }
}

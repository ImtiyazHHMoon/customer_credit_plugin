﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Payments.CustomerCreditPayment.Factories;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Directory;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Components
{
    public class CustomerInvoicePaymentListViewComponent: NopViewComponent
    {
        private readonly ICustomerCreditPaymentService _customerCreditPaymentService;
        private readonly ICustomerCreditPaymentFactory _customerCreditPaymentFactory;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;

        public CustomerInvoicePaymentListViewComponent(ICustomerCreditPaymentService customerCreditPaymentServicec,
            ICustomerCreditPaymentFactory customerCreditPaymentFactory,
            ICurrencyService currencyService, CurrencySettings currencySettings)
        {
            _customerCreditPaymentService = customerCreditPaymentServicec;
            _customerCreditPaymentFactory = customerCreditPaymentFactory;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            if (!(additionalData is CustomerCreditDetailsModel customerCreditDetails))
                return Content("");

            if (customerCreditDetails.NopCustomerId == 0)
                return Content("");

            var currencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;

            var invoicePayment = await _customerCreditPaymentFactory.PreapreCustomerInvoicePaymentSearchModel(new CustomerInvoicePaymentSearchModel
            {
                CustomerId = customerCreditDetails.NopCustomerId
            });


            return View("~/Plugins/Payments.CustomerCreditPayment/Views/CustomerInvoicePaymentList.cshtml", invoicePayment);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Directory;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Components
{
    public class CreditPaymentCustomerDetailsViewComponent: NopViewComponent
    {
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;

        public CreditPaymentCustomerDetailsViewComponent(ICustomerCreditDetailsService customerCreditDetailsService,
            ICurrencyService currencyService, CurrencySettings currencySettings)
        {
            _customerCreditDetailsService = customerCreditDetailsService;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            if (!(additionalData is CustomerModel customer))
                return Content("");

            if (customer.Id == 0)
                return Content("");

            var creditModel = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(customer.Id);
            var currencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;
            if (creditModel == null)
            {
                var newModel = new CustomerCreditDetailsModel
                {
                    Id = 0,
                    NopCustomerId = customer.Id,
                    PrimaryStoreCurrencyCode = currencyCode,
                };
                return View("~/Plugins/Payments.CustomerCreditPayment/Views/CustomerCreditPayment.cshtml", newModel);
            }
            var model = new CustomerCreditDetailsModel
            {
                Id = creditModel.Id,
                AllowOverspend = creditModel.AllowOverspend,
                IsCreditPaymentActive = creditModel.IsCreditPaymentActive,
                AvailableCredit = creditModel.AvailableCredit,
                CreditLimt = creditModel.CreditLimt,
                CreditUsed = creditModel.CreditUsed,
                NopCustomerId = creditModel.NopCustomerId,
                PrimaryStoreCurrencyCode = currencyCode,
            };
            return View("~/Plugins/Payments.CustomerCreditPayment/Views/CustomerCreditPayment.cshtml", model);
        }
    }
}

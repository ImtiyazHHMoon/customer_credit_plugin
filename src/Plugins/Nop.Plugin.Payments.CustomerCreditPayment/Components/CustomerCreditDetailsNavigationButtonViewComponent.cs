﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Components
{
    [ViewComponent]
    public class CustomerCreditDetailsNavigationButtonViewComponent : NopViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("~/Plugins/Payments.CustomerCreditPayment/Views/CustomerNavButton.cshtml");
        }
    }
}

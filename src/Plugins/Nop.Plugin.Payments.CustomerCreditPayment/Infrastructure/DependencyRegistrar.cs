﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Payments.CustomerCreditPayment.Factories;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Common;
using Nop.Web.Factories;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 1000;

        public void Register(IServiceCollection services, ITypeFinder typeFinder, AppSettings appSettings)
        {
            services.AddScoped<IGenericAttributeService, GenericAttributeService>();
            services.AddScoped<ICustomerCreditDetailsService, CustomerCreditDetailsService>();
            services.AddScoped<ICustomerCreditUsedOrderService, CustomerCreditUsedOrderService>();
            services.AddScoped<ICustomerCreditHistoryService, CustomerCreditHistoryService>();
            services.AddScoped<ICustomerCreditPaymentService, CustomerCreditPaymentService>();

            services.AddScoped<ICustomerCreditPaymentFactory, CustomerCreditPaymentFactory>();
            services.AddScoped<ICustomerCreditDetailsFactory, CustomerCreditDetailsFactory>();
            services.AddScoped<ICustomerCreditHistoryFactory, CustomerCreditHistoryFactory>();

            //services.AddScoped<ICustomerModelFactory, B2BCustomerModelFactory>();
        }
    }
}

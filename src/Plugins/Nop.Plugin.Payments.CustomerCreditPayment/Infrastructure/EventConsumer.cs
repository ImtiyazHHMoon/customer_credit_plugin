﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Customers;
using Nop.Services.Events;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Infrastructure
{
    public class EventConsumer : IConsumer<OrderCancelledEvent>
    {
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;
        private readonly ICustomerCreditHistoryService _customerCreditHistoryService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;

        public EventConsumer(ICustomerCreditDetailsService customerCreditDetailsService,
            ICustomerCreditHistoryService customerCreditHistoryService,
            IWorkContext workContext, ICustomerService customerService)
        {
            _customerCreditDetailsService = customerCreditDetailsService;
            _customerCreditHistoryService = customerCreditHistoryService;
            _workContext = workContext;
            _customerService = customerService;
        }
        public async Task HandleEventAsync(OrderCancelledEvent eventMessage)
        {
            var currCustomer =await _workContext.GetCurrentCustomerAsync();
            var currName =await _customerService.GetCustomerFullNameAsync(currCustomer);

            var customerCreditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(eventMessage.Order.CustomerId);
            if (customerCreditDetails != null && customerCreditDetails.IsCreditPaymentActive)
            {
                customerCreditDetails.AvailableCredit += eventMessage.Order.OrderTotal;
                customerCreditDetails.CreditUsed -= eventMessage.Order.OrderTotal;

                await _customerCreditDetailsService.UpdateCreditDetailsAsync(customerCreditDetails);

                var creditHistory = await _customerCreditHistoryService.GetCustomerCreditHistoryAsync(eventMessage.Order.CustomerId);
                var newHistory = new CustomerCreditHistory
                {
                    PrevValueOfCreditUsed = creditHistory.NewValueOfCreditUsed,
                    NewValueOfCreditUsed = creditHistory.NewValueOfCreditUsed - eventMessage.Order.OrderTotal,
                    ActivityDateUtc = DateTime.UtcNow,
                    ActivityTypeId = (int)CreditActivityType.OrderCancelled,
                    NopCustomerId = eventMessage.Order.CustomerId,
                    Description = "Order is cancelled by " + currName,
                };

                await _customerCreditHistoryService.InsertCustomerCreditPaymentAsync(newHistory);
                
            }
        }
    }
}

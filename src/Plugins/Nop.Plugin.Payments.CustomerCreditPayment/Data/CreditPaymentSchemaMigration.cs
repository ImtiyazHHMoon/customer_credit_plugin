﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using Nop.Data.Migrations;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Data
{
    ////[SkipMigrationOnUpdate]
    [NopMigration("2021/12/13 17:05:17:6455422", "Payments.CustomerCreditPayment base schema")]
    public class CreditPaymentSchemaMigration : AutoReversingMigration
    {
        protected IMigrationManager _migrationManager;
        public CreditPaymentSchemaMigration(IMigrationManager migrationManager)
        {
            _migrationManager = migrationManager;
        }
        public override void Up()
        {
            _migrationManager.BuildTable<CustomerCreditDetails>(Create);
            _migrationManager.BuildTable<CustomerCreditUsedOrder>(Create);
            _migrationManager.BuildTable<CustomCreditPayment>(Create);
            _migrationManager.BuildTable<CustomerCreditHistory>(Create);

        }
    }
}

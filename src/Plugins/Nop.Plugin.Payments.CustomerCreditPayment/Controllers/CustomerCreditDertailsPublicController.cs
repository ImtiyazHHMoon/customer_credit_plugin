﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Directory;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class CustomerCreditDertailsPublicController: BasePluginController
    {
        private readonly IWorkContext _workContext;
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;
        private ICurrencyService _currencyService;
        public CustomerCreditDertailsPublicController(IWorkContext workContext, ICustomerCreditDetailsService customerCreditDetailsService,
            ICurrencyService currencyService)
        {
            _workContext = workContext;
            _customerCreditDetailsService = customerCreditDetailsService;
            _currencyService = currencyService;
        }
        [HttpGet]
        public async Task<IActionResult> CreditDetailsPublicView()
        {
            var currCustomer = await _workContext.GetCurrentCustomerAsync();
            var dominModel = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(currCustomer.Id);

            if(dominModel == null)
            {
                return View("~/Plugins/Payments.CustomerCreditPayment/Views/CreditDetailsPublicView.cshtml", new CustomerCreditDetailsModel 
                {
                    IsCreditPaymentActive = false
                });
            }

            var currentCurrencey = await _workContext.GetWorkingCurrencyAsync();
            var availCredit = await _currencyService.ConvertFromPrimaryStoreCurrencyAsync(dominModel.AvailableCredit, currentCurrencey);
            var creditLimit = await _currencyService.ConvertFromPrimaryStoreCurrencyAsync(dominModel.CreditLimt, currentCurrencey);
            var creditUsed = await _currencyService.ConvertFromPrimaryStoreCurrencyAsync(dominModel.CreditUsed, currentCurrencey);
            var currencyCode = currentCurrencey.CurrencyCode;

            var model = new CustomerCreditDetailsModel
            {
                Id = dominModel.Id,
                NopCustomerId = dominModel.NopCustomerId,
                AllowOverspend = dominModel.AllowOverspend,
                AllowOverSpendStr = dominModel.AllowOverspend == true ? "Yes":"No",
                AvailableCredit = availCredit,
                IsCreditPaymentActive = dominModel.IsCreditPaymentActive,
                CreditPaymentEnableStr = dominModel.IsCreditPaymentActive == true ? "Enabled":"Disabled",
                CreditLimt = creditLimit,
                CreditUsed = creditUsed,
                PrimaryStoreCurrencyCode = currencyCode
            };
            return View("~/Plugins/Payments.CustomerCreditPayment/Views/CreditDetailsPublicView.cshtml", model);
        }
    }
}

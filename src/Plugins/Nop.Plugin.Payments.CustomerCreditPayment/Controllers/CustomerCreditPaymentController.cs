﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;
using Nop.Plugin.Payments.CustomerCreditPayment.Factories;
using Nop.Plugin.Payments.CustomerCreditPayment.Models;
using Nop.Plugin.Payments.CustomerCreditPayment.Services;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Models.Extensions;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Controllers
{
    [Area(AreaNames.Admin)]
    [AuthorizeAdmin]
    [AutoValidateAntiforgeryToken]
    public class CustomerCreditPaymentController:BasePluginController
    {
        private readonly IPermissionService _permissionService;
        private readonly ICustomerCreditDetailsService _customerCreditDetailsService;
        private readonly ICustomerCreditPaymentService _customerCreditPaymentService;
        private readonly ICustomerCreditHistoryService _customerCreditHistoryService;
        private readonly INotificationService _notificationService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly ICustomerCreditPaymentFactory _customerCreditPaymentFactory;
        private readonly ICustomerCreditDetailsFactory _customerCreditDetailsFactory;
        private readonly ICustomerCreditHistoryFactory _customerCreditHistoryFactory;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly ICustomerService _customerService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;

        public CustomerCreditPaymentController(IPermissionService permissionService, ICustomerCreditDetailsService customerCreditDetailsService,
            INotificationService notificationService, IStoreContext storeContext, ISettingService settingService,
            ICustomerCreditPaymentFactory customerCreditPaymentFactory, ILocalizationService localizationService,
            ICustomerCreditPaymentService customerCreditPaymentService, ICustomerCreditHistoryService customerCreditHistoryService,
            ICustomerCreditDetailsFactory customerCreditDetailsFactory, ICustomerCreditHistoryFactory customerCreditHistoryFactory,
            IWorkContext workContext, ICustomerService customerService, ICurrencyService currencyService, CurrencySettings currencySettings)
        {
            _permissionService = permissionService;
            _customerCreditDetailsService = customerCreditDetailsService;
            _notificationService = notificationService;
            _storeContext = storeContext;   
            _settingService = settingService;
            _customerCreditPaymentFactory = customerCreditPaymentFactory;
            _customerCreditDetailsFactory = customerCreditDetailsFactory;
            _localizationService = localizationService;
            _customerCreditPaymentService = customerCreditPaymentService;
            _customerCreditHistoryService = customerCreditHistoryService;
            _customerCreditHistoryFactory = customerCreditHistoryFactory;
            _workContext = workContext;
            _customerService = customerService;
            _currencyService = currencyService;
            _currencySettings = currencySettings;
        }


        [HttpPost]
        public async Task<IActionResult> InvoiceList(CustomerInvoicePaymentSearchModel serachModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var model = await _customerCreditPaymentFactory.PrepareCustomCreditPaymentListModel(serachModel);
            return Json(model);
        }

        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var cpSettings = await _settingService.LoadSettingAsync<CustomerCreditPaymentSettings>(storeScope);
            var currencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;
            var model = new ConfigurationModel
            {
                DescriptionText = cpSettings.DescriptionText,
                SkipPaymentInfo = cpSettings.SkipPaymentInfo,
                MinimumCredit = cpSettings.MinimumCredit,
                ActiveStoreScopeConfiguration = storeScope,
                PrimaryStoreCurrencyCode = currencyCode
            };

            return View("~/Plugins/Payments.CustomerCreditPayment/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> Configure(ConfigurationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return await Configure();

            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var cpSettings = await _settingService.LoadSettingAsync<CustomerCreditPaymentSettings>(storeScope);

            cpSettings.DescriptionText = model.DescriptionText;
            cpSettings.SkipPaymentInfo = model.SkipPaymentInfo;
            cpSettings.MinimumCredit = model.MinimumCredit;

            await _settingService.SaveSettingAsync(cpSettings);
            await _settingService.SaveSettingOverridablePerStoreAsync(cpSettings, x => x.DescriptionText, model.DescriptionText_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(cpSettings, x => x.SkipPaymentInfo, model.SkipPaymentInfo_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(cpSettings, x => x.MinimumCredit, model.MinimumCredit_OverrideForStore, storeScope, false);

            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            return await Configure();
        }

        [HttpGet]
        public async Task<IActionResult> CustomerCreditDetails(int Id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            var model = _customerCreditDetailsService.GetCreditDetailsByIdAsync(Id);
            if(model != null)
                return Json(model);

            return null;// test purpose
        }

        [HttpPost]
        public async Task<IActionResult> Create(CustomerCreditDetailsModel customerCreditDetailsModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            if(customerCreditDetailsModel == null)
                return StatusCode(401);

            var currCustomer = await _workContext.GetCurrentCustomerAsync();
            var currCustomerName = await _customerService.GetCustomerFullNameAsync(currCustomer);
            var currencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;

            var model = new CustomerCreditDetails();
            var creditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(customerCreditDetailsModel.NopCustomerId);
            if (creditDetails == null)
            {

                var createModel = new CustomerCreditDetails
                {
                    NopCustomerId = customerCreditDetailsModel.NopCustomerId,
                    CreditLimt = customerCreditDetailsModel.CreditLimt,
                    CreditUsed = customerCreditDetailsModel.CreditUsed,
                    AvailableCredit = customerCreditDetailsModel.AvailableCredit,
                    IsCreditPaymentActive = customerCreditDetailsModel.IsCreditPaymentActive,
                    AllowOverspend = customerCreditDetailsModel.AllowOverspend,
                };

                await _customerCreditDetailsService.InsertCreditDetailsAsync(createModel);
                model = createModel;
            }
            else
            {
                creditDetails.AllowOverspend = customerCreditDetailsModel.AllowOverspend;
                creditDetails.IsCreditPaymentActive = customerCreditDetailsModel.IsCreditPaymentActive;
                creditDetails.CreditUsed = customerCreditDetailsModel.CreditUsed;
                creditDetails.CreditLimt = customerCreditDetailsModel.CreditLimt;
                creditDetails.AvailableCredit = customerCreditDetailsModel.AvailableCredit;

                await _customerCreditDetailsService.UpdateCreditDetailsAsync(creditDetails);
                model = creditDetails;
            }

            /// update credit history
            var prevHistory = await _customerCreditHistoryService.GetCustomerCreditHistoryAsync(model.NopCustomerId);
            var creditHistory = new CustomerCreditHistory();

            creditHistory.ActivityDateUtc = DateTime.UtcNow;
            creditHistory.ActivityTypeId = (int)CreditActivityType.InvoicePayment;
            creditHistory.NopCustomerId = model.NopCustomerId;
            creditHistory.PrevValueOfCreditUsed = prevHistory != null ? prevHistory.PrevValueOfCreditUsed : 0;
            creditHistory.NewValueOfCreditUsed = prevHistory != null ? prevHistory.NewValueOfCreditUsed : 0;
            creditHistory.Description = "Customer credit information updated by " + currCustomerName;
            creditHistory.UsedCurrency = currencyCode;

            await _customerCreditHistoryService.InsertCustomerCreditPaymentAsync(creditHistory);


            await _localizationService.AddOrUpdateLocaleResourceAsync("Plugins.Payments.CustomerCreditPayment.AvailableCredit", model.AvailableCredit.ToString());

            //_notificationService.SuccessNotification("Credit details saved!");

            return Json(new { Result = true });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CustomerCreditDetailsModel customerCreditDetailsModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            var creditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(customerCreditDetailsModel.NopCustomerId);
            if (creditDetails == null)
                return NotFound();

            creditDetails.AllowOverspend = customerCreditDetailsModel.AllowOverspend;
            creditDetails.IsCreditPaymentActive = customerCreditDetailsModel.IsCreditPaymentActive;
            creditDetails.CreditUsed = customerCreditDetailsModel.CreditUsed;
            creditDetails.CreditLimt = customerCreditDetailsModel.CreditLimt;
            creditDetails.AvailableCredit = customerCreditDetailsModel.AvailableCredit;

            await _customerCreditDetailsService.UpdateCreditDetailsAsync(creditDetails);

            return Ok();
        }

        [HttpGet]
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            var creditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(id);
            if (creditDetails == null)
                return NotFound();

            await _customerCreditDetailsService.DeleteCreditDetailsAsync(creditDetails);

            return Ok("Deleted successfully");
        }

        
        [HttpGet]
        public async Task<IActionResult> InvoicePaymentCreate(int customerId)
        {
            var currencyCode = (await _currencyService.GetCurrencyByIdAsync(_currencySettings.PrimaryStoreCurrencyId))?.CurrencyCode;

            var model = new CustomCreditPaymentModel
            {
                NopCustomerId = customerId,
                PrimaryStoreCurrencyCode = currencyCode
            };
            return View("~/Plugins/Payments.CustomerCreditPayment/Views/InvoicePaymentCreate.cshtml", model);            
        }

        [HttpPost]
        public async Task<IActionResult> InvoicePaymentCreate(CustomCreditPaymentModel customCreditPaymentModel)
        {
            if (customCreditPaymentModel == null || customCreditPaymentModel.Amount == 0)
                return await InvoicePaymentCreate(customCreditPaymentModel.NopCustomerId);

            var creditDetails = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(customCreditPaymentModel.NopCustomerId);
            if (creditDetails == null)
            {
                _notificationService.ErrorNotification("Customer is not eligible for credit payment");
                return await InvoicePaymentCreate(customCreditPaymentModel.NopCustomerId);
            }
            var currCustomer = await _workContext.GetCurrentCustomerAsync();
            var currCustomerName = await _customerService.GetCustomerFullNameAsync(currCustomer);
            var currencyCode = await _workContext.GetWorkingCurrencyAsync();
            var model = new CustomCreditPayment();
            if (customCreditPaymentModel.Id == 0)
            {

                model = new CustomCreditPayment
                {
                    Amount = customCreditPaymentModel.Amount,
                    CreatedById = currCustomer.Id,
                    InvoiceReference = customCreditPaymentModel.InvoiceReference,
                    PaymentDateUtc = customCreditPaymentModel.PaymentDateUtc,
                    //EditedById = customCreditPaymentModel.EditedById,
                    NopCustomerId = customCreditPaymentModel.NopCustomerId,
                };

                await _customerCreditPaymentService.InsertInvoicePaymentAsync(model);

                /// update credit details
                var creditModel = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(model.NopCustomerId);
                if (creditModel == null)
                    return BadRequest();

                creditModel.AvailableCredit += model.Amount;
                await _customerCreditDetailsService.UpdateCreditDetailsAsync(creditModel);

                /// update credit history
                /// 
                var prevHistory = await _customerCreditHistoryService.GetCustomerCreditHistoryAsync(model.NopCustomerId);

                var creditHistory = new CustomerCreditHistory();

                creditHistory.ActivityDateUtc = DateTime.UtcNow;
                creditHistory.ActivityTypeId = (int)CreditActivityType.InvoicePayment;
                creditHistory.NopCustomerId = model.NopCustomerId;
                creditHistory.PrevValueOfCreditUsed = prevHistory != null ? prevHistory.PrevValueOfCreditUsed : 0;
                creditHistory.NewValueOfCreditUsed = prevHistory != null ? prevHistory.NewValueOfCreditUsed : 0;
                creditHistory.Description = "Invoice payment creted by " + currCustomerName;
                creditHistory.UsedCurrency = currencyCode.CurrencyCode;

                await _customerCreditHistoryService.InsertCustomerCreditPaymentAsync(creditHistory);
                _notificationService.SuccessNotification("Invoice payment added to the customer");
            }
            else
            {
                var prevInvoice = await _customerCreditPaymentService.GetCustomerInvoiceAsync(customCreditPaymentModel.Id);
                if (prevInvoice == null)
                    return NotFound();

                decimal prevAmount = prevInvoice.Amount;

                prevInvoice.InvoiceReference = customCreditPaymentModel.InvoiceReference;
                prevInvoice.Amount = customCreditPaymentModel.Amount;
                prevInvoice.EditedById = currCustomer.Id;
                prevInvoice.PaymentDateUtc = customCreditPaymentModel.PaymentDateUtc;
                

                await _customerCreditPaymentService.UpdateInvoicePaymentAsync(prevInvoice);

                /// update credit details
                var creditModel = await _customerCreditDetailsService.GetCreditDetailsByIdAsync(prevInvoice.NopCustomerId);
                if (creditModel == null)
                    return BadRequest();

                // recalculating the adjustment
                creditDetails.AvailableCredit = creditDetails.AvailableCredit - prevAmount;
                creditDetails.AvailableCredit += customCreditPaymentModel.Amount;

                await _customerCreditDetailsService.UpdateCreditDetailsAsync(creditDetails);

                // add adjustment history
                var prevHistory = await _customerCreditHistoryService.GetCustomerCreditHistoryAsync(creditDetails.NopCustomerId);
                var creditHistory = new CustomerCreditHistory
                {
                    NopCustomerId = creditDetails.NopCustomerId,
                    ActivityDateUtc = DateTime.UtcNow,
                    ActivityTypeId = (int)CreditActivityType.InvoiceAdjustment,
                    PrevValueOfCreditUsed = prevHistory != null ? prevHistory.PrevValueOfCreditUsed:0,
                    NewValueOfCreditUsed = prevHistory != null ? prevHistory.NewValueOfCreditUsed : 0,
                    CreditUsedAdjustment = Math.Abs(prevAmount - customCreditPaymentModel.Amount),
                    Description = "Credit adjusted by " + currCustomerName,
                    UsedCurrency = currencyCode.CurrencyCode,
            };

                await _customerCreditHistoryService.InsertCustomerCreditPaymentAsync(creditHistory);

                _notificationService.SuccessNotification("Invoice payment adjust to the customer");
            }

            

            return await InvoicePaymentCreate(model.NopCustomerId);
        }

        public async Task<IActionResult> InvoicePaymentEdit(int id)
        {
            var invoice = await _customerCreditPaymentService.GetCustomerInvoiceAsync(id);

            if (invoice != null)
            {
                var model = new CustomCreditPaymentModel
                {
                    Id = invoice.Id,
                    Amount = invoice.Amount,
                    NopCustomerId = invoice.NopCustomerId,
                    CreatedById = invoice.CreatedById,
                    EditedById = invoice.EditedById,
                    InvoiceReference = invoice.InvoiceReference,
                    PaymentDateUtc = invoice.PaymentDateUtc
                };
                return View("~/Plugins/Payments.CustomerCreditPayment/Views/InvoicePaymentCreate.cshtml", model);
            }
            else
                return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> CreditHistoryList(CustomerCreditHistorySearchModel searchModel)
        {
            var listModel = await _customerCreditHistoryFactory.PrepareCustomerCreditHistoryListModel(searchModel);
            return Json(listModel);
        }

        [HttpGet]
        public async Task<IActionResult> EditCreditDetails(CustomerCreditDetailsSearchModel searchModel)
        {
            return Json(searchModel);
        }

        [HttpGet]
        public async Task<IActionResult> AllCreditCustomerList()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return await AccessDeniedDataTablesJson();

            var searchModel = await _customerCreditDetailsFactory.PrepareCustomerCreditDetailsSearchModel(new CustomerCreditDetailsSearchModel());
            return View("~/Plugins/Payments.CustomerCreditPayment/Views/AllCreditCustomerList.cshtml", searchModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreditDetailsSearch(CustomerCreditDetailsSearchModel searchModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return await AccessDeniedDataTablesJson();

            var model = await _customerCreditDetailsFactory.PrepareCustomerCreditDetailsSearchModel(searchModel);

            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> ListOfCreditCustomers(CustomerCreditDetailsSearchModel searchModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageCustomers))
                return await AccessDeniedDataTablesJson();

            var listModel = await _customerCreditDetailsFactory.PrepareCustomerCreditDetailsListModelWithQuery(searchModel);

            return Json(listModel);
        }

        
    }
}

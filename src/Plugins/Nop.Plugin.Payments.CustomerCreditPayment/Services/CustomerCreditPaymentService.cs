﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Data;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public class CustomerCreditPaymentService : ICustomerCreditPaymentService
    {
        private readonly CacheKey _creditpaymentkey = new CacheKey("Nop.creditpaymentkey.all-{0}", CREDIT_INVOICE_PAYMENT_PATTERN_KEY);
        private const string CREDIT_INVOICE_PAYMENT_PATTERN_KEY = "Nop.creditpaymentkey.";

        private readonly IRepository<CustomCreditPayment> _customerCreditPaymentRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public CustomerCreditPaymentService(IRepository<CustomCreditPayment> repository, IStaticCacheManager staticCacheManager)
        {
            _customerCreditPaymentRepository = repository;
            _staticCacheManager = staticCacheManager;
        }

        public async Task DeleteInvoicePaymentAsync(CustomCreditPayment customCreditPayment)
        {
            await _customerCreditPaymentRepository.DeleteAsync(customCreditPayment);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_INVOICE_PAYMENT_PATTERN_KEY);
        }

        public async Task<IPagedList<CustomCreditPayment>> GetCustomerInvoicePaymentList(int customerId)
        {
            return await _customerCreditPaymentRepository.GetAllPagedAsync(query =>
            {
                query = query.Where(q => q.NopCustomerId == customerId);
                return query;
            });
        }

        public async Task InsertInvoicePaymentAsync(CustomCreditPayment customCreditPayment)
        {
            await _customerCreditPaymentRepository.InsertAsync(customCreditPayment);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_INVOICE_PAYMENT_PATTERN_KEY);
        }

        public async Task UpdateInvoicePaymentAsync(CustomCreditPayment customerCreditPayment)
        {
            await _customerCreditPaymentRepository.UpdateAsync(customerCreditPayment);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_INVOICE_PAYMENT_PATTERN_KEY);
        }

        public async Task<CustomCreditPayment> GetCustomerInvoiceAsync(int invoiceId)
        {
            var query = from i in _customerCreditPaymentRepository.Table
                         where i.Id == invoiceId
                         select i;
            var result = await query.FirstOrDefaultAsync();
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Data;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public class CustomerCreditUsedOrderService : ICustomerCreditUsedOrderService
    {


        private readonly CacheKey _pickupPointAllKey = new CacheKey("Nop.creditusedorder.all-{0}", CREDIT_USED_ORDER_PATTERN_KEY);
        private const string CREDIT_USED_ORDER_PATTERN_KEY = "Nop.creditusedorder.";

        private readonly IRepository<CustomerCreditUsedOrder> _customerCreditUsedOrderRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public CustomerCreditUsedOrderService(IRepository<CustomerCreditUsedOrder> customerCreditUsedOrderRepository,
            IStaticCacheManager staticCacheManager)
        {
            _customerCreditUsedOrderRepository = customerCreditUsedOrderRepository;
            _staticCacheManager = staticCacheManager;
        }


        public async Task DeleteCreditUsedOrderAsync(CustomerCreditUsedOrder model)
        {
            await _customerCreditUsedOrderRepository.DeleteAsync(model);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_USED_ORDER_PATTERN_KEY);
        }

        public async Task<IList<CustomerCreditUsedOrder>> GetCustomerCreditUsedOrderListAsync(int nopCustomerId)
        {
            return await _customerCreditUsedOrderRepository.GetAllAsync(query =>
            {
                query = query.Where(q => q.NopCustomerId == nopCustomerId);
                return query;
            });
        }

        public async Task InsertCreditUsedOrderAsync(CustomerCreditUsedOrder customerCreditUsedOrder)
        {
            await _customerCreditUsedOrderRepository.InsertAsync(customerCreditUsedOrder);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_USED_ORDER_PATTERN_KEY);
        }

        public async Task UpdateCreditUsedOrderAsync(CustomerCreditUsedOrder customerCreditUsedOrder)
        {
            await _customerCreditUsedOrderRepository.UpdateAsync(customerCreditUsedOrder,false);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_USED_ORDER_PATTERN_KEY);
        }

        public async Task<CustomerCreditUsedOrder> GetCustomerCreditUsedOrderAsync(Guid orderGuid)
        {
            var query = from c in _customerCreditUsedOrderRepository.Table                        
                        where c.OrderGuid == orderGuid
                        select c;
            var customerCreditUsedOrder = await query.FirstOrDefaultAsync();

            return customerCreditUsedOrder;
        }
    }
}

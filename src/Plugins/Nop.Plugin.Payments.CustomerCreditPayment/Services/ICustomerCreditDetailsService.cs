﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public interface ICustomerCreditDetailsService
    {
        Task InsertCreditDetailsAsync(CustomerCreditDetails model);
        Task UpdateCreditDetailsAsync(CustomerCreditDetails model);
        Task DeleteCreditDetailsAsync(CustomerCreditDetails model);
        Task<CustomerCreditDetails> GetCreditDetailsByIdAsync(int nopCustomerId);
        Task<IPagedList<CustomerCreditDetails>> GetAllCreditedCustomer();
        Task<IPagedList<CustomerCreditDetails>> GetAllCreditedCustomerWithQuery(int[] customerRoleIds = null,
            string email = null, string firstName = null, string lastName = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Data;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;
using Nop.Services.Customers;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public class CustomerCreditDetailsService : ICustomerCreditDetailsService
    {

        private readonly CacheKey _pickupPointAllKey = new CacheKey("Nop.creditpaymentdetail.all-{0}", CREDIT_PAYMENT_PATTERN_KEY);
        private const string CREDIT_PAYMENT_PATTERN_KEY = "Nop.creditpaymentdetail.";

        private readonly IRepository<CustomerCreditDetails> _customerCreditDetailsRepository;
        private readonly IRepository<CustomerCustomerRoleMapping> _customerCustomerRoleMappingRepository;
        private readonly IRepository<GenericAttribute> _gaRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IRepository<Customer> _customerRepository;

        public CustomerCreditDetailsService(IRepository<CustomerCreditDetails> customerCreditPaymentRepository, 
            IStaticCacheManager staticCacheManager, IRepository<CustomerCustomerRoleMapping> customerCustomerRoleMappingRepository,
            IRepository<GenericAttribute> gaRepository, IRepository<Customer> customerService)
        {
            _customerCreditDetailsRepository = customerCreditPaymentRepository;
            _customerCustomerRoleMappingRepository = customerCustomerRoleMappingRepository;
            _gaRepository = gaRepository;
            _staticCacheManager = staticCacheManager;
            _customerRepository = customerService;
        }

        public async Task InsertCreditDetailsAsync(CustomerCreditDetails model)
        {
            await _customerCreditDetailsRepository.InsertAsync(model,false);
            //await _staticCacheManager.RemoveByPrefixAsync(CREDIT_PAYMENT_PATTERN_KEY);
        }

        public async Task UpdateCreditDetailsAsync(CustomerCreditDetails model)
        {
            await _customerCreditDetailsRepository.UpdateAsync(model,false);
            //await _staticCacheManager.RemoveByPrefixAsync(CREDIT_PAYMENT_PATTERN_KEY);
        }

        public async Task DeleteCreditDetailsAsync(CustomerCreditDetails model)
        {
            await _customerCreditDetailsRepository.DeleteAsync(model,false);
            //await _staticCacheManager.RemoveByPrefixAsync(CREDIT_PAYMENT_PATTERN_KEY);
        }

        public async Task<CustomerCreditDetails> GetCreditDetailsByIdAsync(int nopCustomerId)
        {
            var query = from c in _customerCreditDetailsRepository.Table
                        orderby c.Id
                        where c.NopCustomerId == nopCustomerId
                        select c;
            var customerCreditDetails = await query.FirstOrDefaultAsync();

            return customerCreditDetails;
        }

        public async Task<IPagedList<CustomerCreditDetails>> GetAllCreditedCustomer()
        {
            var query = from c in _customerCreditDetailsRepository.Table
                        orderby c.Id
                        //where c.IsCreditPaymentActive == true
                        select c;

            var allCreditedCustomers = await query.ToPagedListAsync(0,int.MaxValue);

            return allCreditedCustomers;
        }

        public async Task<IPagedList<CustomerCreditDetails>> GetAllCreditedCustomerWithQuery(int[] customerRoleIds = null,
            string email = null, string firstName = null, string lastName = null, int pageIndex = 0, int pageSize = int.MaxValue, bool getOnlyTotalCount = false)
        {
            var allCreditedCustomers =await _customerCreditDetailsRepository.GetAllPagedAsync(query =>
            {
                
                if (customerRoleIds != null && customerRoleIds.Length > 0)
                {
                    query = query.Join(_customerCustomerRoleMappingRepository.Table, x => x.NopCustomerId, y => y.CustomerId,
                            (x, y) => new { Customer = x, Mapping = y })
                        .Where(z => customerRoleIds.Contains(z.Mapping.CustomerRoleId))
                        .Select(z => z.Customer)
                        .Distinct();
                }

                if (!string.IsNullOrWhiteSpace(email))
                {
                    //query = query.Where(c => c.Email.Contains(email));

                    query = query.Join(_customerRepository.Table, x => x.NopCustomerId, y => y.Id,
                        (x, y) => new { CreditDetails = x, Customer = y })
                    .Where(z => z.Customer.Email.Contains(email))
                    .Select(z => z.CreditDetails);
                }
                if (!string.IsNullOrWhiteSpace(firstName))
                {
                    query = query
                        .Join(_gaRepository.Table, x => x.NopCustomerId, y => y.EntityId,
                            (x, y) => new { Customer = x, Attribute = y })
                        .Where(z => z.Attribute.KeyGroup == nameof(Customer) &&
                                    z.Attribute.Key == NopCustomerDefaults.FirstNameAttribute &&
                                    z.Attribute.Value.Contains(firstName))
                        .Select(z => z.Customer);
                }

                if (!string.IsNullOrWhiteSpace(lastName))
                {
                    query = query
                        .Join(_gaRepository.Table, x => x.NopCustomerId, y => y.EntityId,
                            (x, y) => new { Customer = x, Attribute = y })
                        .Where(z => z.Attribute.KeyGroup == nameof(Customer) &&
                                    z.Attribute.Key == NopCustomerDefaults.LastNameAttribute &&
                                    z.Attribute.Value.Contains(lastName))
                        .Select(z => z.Customer);
                }

                return query;
            });

            return allCreditedCustomers;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public interface ICustomerCreditPaymentService
    {
        Task InsertInvoicePaymentAsync(CustomCreditPayment customCreditPayment);
        Task UpdateInvoicePaymentAsync(CustomCreditPayment customerCreditPayment);
        Task DeleteInvoicePaymentAsync(CustomCreditPayment customCreditPayment);

        Task<IPagedList<CustomCreditPayment>> GetCustomerInvoicePaymentList(int customerId);
        Task<CustomCreditPayment> GetCustomerInvoiceAsync(int invoiceId); 
    }
}

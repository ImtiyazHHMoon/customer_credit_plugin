﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Data;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public class CustomerCreditHistoryService : ICustomerCreditHistoryService
    {
        private readonly CacheKey _pickupPointAllKey = new CacheKey("Nop.credithistory.all-{0}", CREDIT_HISTORY_PATTERN_KEY);
        private const string CREDIT_HISTORY_PATTERN_KEY = "Nop.credithistory.";

        private readonly IRepository<CustomerCreditHistory> _customerCreditHistoryRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public CustomerCreditHistoryService(IRepository<CustomerCreditHistory> repository,  IStaticCacheManager staticCacheManager )
        {
            _customerCreditHistoryRepository = repository;
            _staticCacheManager = staticCacheManager;
        }
        public async Task DeleteCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory)
        {
            await _customerCreditHistoryRepository.InsertAsync(customerCreditHistory);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_HISTORY_PATTERN_KEY);
        }

        public async Task<CustomerCreditHistory> GetCustomerCreditHistoryAsync(int customerId)
        {
            var query = from c in _customerCreditHistoryRepository.Table
                        orderby c.ActivityDateUtc descending
                        where c.NopCustomerId == customerId
                        select c;
            var customerCreditHistory = await query.FirstOrDefaultAsync();

            return customerCreditHistory;
        }

        public async Task InsertCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory)
        {
            await _customerCreditHistoryRepository.InsertAsync(customerCreditHistory);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_HISTORY_PATTERN_KEY);
        }

        public async Task UpdateCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory)
        {
            await _customerCreditHistoryRepository.UpdateAsync(customerCreditHistory);
            await _staticCacheManager.RemoveByPrefixAsync(CREDIT_HISTORY_PATTERN_KEY);
        }

        public async Task<IPagedList<CustomerCreditHistory>> GetCustomerAllCreditHistoryAsync(int customerId)
        {
            var query = from c in _customerCreditHistoryRepository.Table
                        orderby c.ActivityDateUtc descending
                        where c.NopCustomerId == customerId
                        select c;
            var customerCreditHistory = await query.ToPagedListAsync(0,int.MaxValue);

            return customerCreditHistory;
        }
    }
}

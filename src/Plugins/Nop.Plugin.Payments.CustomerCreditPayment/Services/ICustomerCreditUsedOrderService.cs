﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public interface ICustomerCreditUsedOrderService
    {
        Task InsertCreditUsedOrderAsync(CustomerCreditUsedOrder customerCreditUsedOrder);
        Task DeleteCreditUsedOrderAsync(CustomerCreditUsedOrder model);
        Task UpdateCreditUsedOrderAsync(CustomerCreditUsedOrder customerCreditUsedOrder);
        Task<IList<CustomerCreditUsedOrder>> GetCustomerCreditUsedOrderListAsync(int nopCustomerId); 
        Task<CustomerCreditUsedOrder> GetCustomerCreditUsedOrderAsync(Guid orderGuid); 
    }
}

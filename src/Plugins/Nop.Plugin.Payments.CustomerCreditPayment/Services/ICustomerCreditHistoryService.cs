﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Services
{
    public interface ICustomerCreditHistoryService
    {
        Task InsertCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory);
        Task UpdateCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory);
        Task DeleteCustomerCreditPaymentAsync(CustomerCreditHistory customerCreditHistory);
        Task<CustomerCreditHistory> GetCustomerCreditHistoryAsync(int customerId);
        Task<IPagedList<CustomerCreditHistory>> GetCustomerAllCreditHistoryAsync(int customerId);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Domain
{
    public class CustomerCreditDetails : BaseEntity
    {
        public int NopCustomerId { get; set; }
        public bool IsCreditPaymentActive { get; set; }
        public decimal CreditLimt { get; set; }
        public decimal CreditUsed { get; set; }
        public decimal AvailableCredit { get; set; }
        public bool AllowOverspend { get; set; }

    }
}

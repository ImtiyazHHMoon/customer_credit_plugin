﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Domain
{
    public class CustomerCreditUsedOrder : BaseEntity
    {
        public int NopCustomerId { get; set; }
        public Guid OrderGuid { get; set; }
        public decimal OrderTotal { get; set; }
        public string OrderReference { get; set; }
        public string UsedCurrency { get; set; }
    }
}

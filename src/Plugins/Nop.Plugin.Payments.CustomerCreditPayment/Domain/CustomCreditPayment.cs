﻿using System;
using Nop.Core;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Domain
{
    public class CustomCreditPayment : BaseEntity
    {
        public int NopCustomerId { get; set; }
        public string InvoiceReference { get; set; }
        public DateTime PaymentDateUtc { get; set; }
        public decimal Amount { get; set; }
        public int CreatedById { get; set; }
        public int EditedById { get; set; }
    }
}
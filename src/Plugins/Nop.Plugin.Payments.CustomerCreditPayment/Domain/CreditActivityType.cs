﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Domain
{
    public enum CreditActivityType
    {
        OrderPlaced = 1,
        OrderCancelled = 2,
        AdminModify = 3,
        InvoicePayment = 4,
        InvoiceAdjustment = 5
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Domain
{
    public class CustomerCreditHistory: BaseEntity
    {
        public int NopCustomerId { get; set; }
        public int ActivityTypeId { get; set; }
        public decimal PrevValueOfCreditUsed { get; set; }
        public decimal CreditUsedAdjustment { get; set; }
        public decimal NewValueOfCreditUsed { get; set; }
        public DateTime ActivityDateUtc { get; set; }
        public string Description { get; set; }
        public string UsedCurrency { get; set; }

        public CreditActivityType ActivityType
        { 
          get=> (CreditActivityType)ActivityTypeId; 
          set=> ActivityTypeId=(int)value; 
        }
    }
}

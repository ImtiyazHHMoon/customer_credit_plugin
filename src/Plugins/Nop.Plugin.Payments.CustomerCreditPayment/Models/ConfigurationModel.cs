﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record ConfigurationModel:BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.PaymentMethodDescription")]
        public string DescriptionText { get; set; }
        public bool DescriptionText_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.SkipPaymentInfo")]
        public bool SkipPaymentInfo { get; set; }
        public bool SkipPaymentInfo_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.MinimumCredit")]
        public decimal MinimumCredit { get; set; }
        public bool MinimumCredit_OverrideForStore { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }
    }
}

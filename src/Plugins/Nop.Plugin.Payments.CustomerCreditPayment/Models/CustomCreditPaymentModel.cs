﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomCreditPaymentModel:BaseNopEntityModel
    {
        public int NopCustomerId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.InvoiceReference")]
        public string InvoiceReference { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.PaymentDateUtc")]
        public DateTime PaymentDateUtc { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.Amount")]
        public decimal Amount { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.CreatedById")]
        public int CreatedById { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.EditedById")]
        public int EditedById { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }

    }
}

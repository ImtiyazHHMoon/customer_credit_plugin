﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomerCreditDetailsSearchModel: BaseSearchModel
    {
        public CustomerCreditDetailsSearchModel()
        {
            SelectedCustomerRoleIds = new List<int>();
            AvailableCustomerRoles = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.List.FirstName")]
        public string FirstName { get; set; }
        public bool FirstNameEnabled { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.List.LastName")]
        public string LastName { get; set; }
        public bool LastNameEnabled { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.List.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.List.CustomerRoles")]
        public IList<int> SelectedCustomerRoleIds { get; set; }

        public IList<SelectListItem> AvailableCustomerRoles { get; set; }

    }
}

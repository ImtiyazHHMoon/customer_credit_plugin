﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Payments.CustomerCreditPayment.Domain;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomerCreditHistoryModel : BaseNopEntityModel
    {
        public int NopCustomerId { get; set; }
        public int ActivityTypeId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.PrevValueOfCreditUsed")]
        public decimal PrevValueOfCreditUsed { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.CreditUsedAdjustment")]
        public decimal CreditUsedAdjustment { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.NewValueOfCreditUsed")]
        public decimal NewValueOfCreditUsed { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.ActivityDateUtc")]
        public DateTime ActivityDateUtc { get; set; }
        public string Description { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.UsedCurrency")]
        public string UsedCurrency { get; set; }


        public CreditActivityType ActivityType
        {
            get => (CreditActivityType)ActivityTypeId;
            set => ActivityTypeId = (int)value;
        }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.ActivityType")]
        public string ActivityName 
        { 
            get => ActivityType.ToString(); 
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomerCreditHistorySearchModel: BaseSearchModel
    {
        public int CustomerId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomerCreditDetailsModel : BaseNopEntityModel
    {
        public int NopCustomerId { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.IsCreditPaymentActive")]
        public bool IsCreditPaymentActive { get; set; }

        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.CreditLimt")]
        public decimal CreditLimt { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.CreditUsed")]
        public decimal CreditUsed { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.AvailableCredit")]
        public decimal AvailableCredit { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.AllowOverspend")]
        public bool AllowOverspend { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.FullName")]
        public string FullName { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.Email")]
        public string Email { get; set; }
        [NopResourceDisplayName("Plugins.Payments.CustomerCreditPayment.Fields.CustomerRole")]
        public string CustomerRole { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }
        public string AllowOverSpendStr { get; set; }
        public string CreditPaymentEnableStr { get; set; }
    }
}

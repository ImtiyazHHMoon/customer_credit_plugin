﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record CustomerCreditUsedOrderModel: BaseNopEntityModel
    {
        public int NopCustomerId { get; set; }
        public Guid OrderGuid { get; set; }
        public decimal OrderTotal { get; set; }
        public string OrderReference { get; set; }
        public string UsedCurrency { get; set; }
    }
}

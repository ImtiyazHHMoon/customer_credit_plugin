﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.CustomerCreditPayment.Models
{
    public record PaymentInfoModel : BaseNopModel
    {
        public string DescriptionText { get; set; }
        public string AvailableCredit { get; set; }
        public string OrderReference { get; set; }
        public string PrimaryStoreCurrencyCode { get; set; }
        public bool HaveWarningMsg { get; set; }
        public string WarningMsg { get; set; }
    }
}

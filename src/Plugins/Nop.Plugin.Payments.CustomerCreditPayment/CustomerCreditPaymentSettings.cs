﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.CustomerCreditPayment
{
    public class CustomerCreditPaymentSettings:ISettings
    {
        public string DescriptionText { get; set; }

        public decimal MinimumCredit { get; set; }

        public bool SkipPaymentInfo { get; set; }
    }
}

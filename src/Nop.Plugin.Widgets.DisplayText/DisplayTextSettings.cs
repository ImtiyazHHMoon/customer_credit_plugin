﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.DisplayText
{
    public class DisplayTextSettings : ISettings
    {
        public string DisplayText { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Widgets.DisplayText.Models
{
    public record DisplayTextModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.DisplayText.TextToDisplay")]
        public string TextToDisplay { get; set; }
        public bool TextToDisplay_OverrideForStore { get; set; }
    }
}

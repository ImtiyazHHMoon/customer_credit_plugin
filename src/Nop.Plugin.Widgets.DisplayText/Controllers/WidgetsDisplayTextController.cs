﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.DisplayText.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Widgets.DisplayText.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class WidgetsDisplayTextController : BasePluginController
    {
        private readonly IPermissionService _permissionService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly INotificationService _notificationService;
        private readonly ILocalizationService _localizationService;


        public WidgetsDisplayTextController(IPermissionService permissionService, IStoreContext storeContext, ISettingService settingService,
            INotificationService notificationService, ILocalizationService localizationService)
        {
            _permissionService = permissionService;
            _storeContext = storeContext;
           _settingService = settingService;
            _notificationService = notificationService;
            _localizationService = localizationService;
        }
        [HttpGet]
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var storeScope =await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var displayTextSettings = await _settingService.LoadSettingAsync<DisplayTextSettings>(storeScope);

            var model = new DisplayTextModel
            {
                TextToDisplay = displayTextSettings.DisplayText
            };

            if(storeScope > 0)
            {
                model.TextToDisplay_OverrideForStore = await _settingService.SettingExistsAsync(displayTextSettings, x => x.DisplayText, storeScope);
            }


            return View("~/Plugins/Widgets.DisplayText/Views/Configure.cshtml", model);
        }

        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Configure(DisplayTextModel model)
        {

            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();


            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var displayTextSettings = await _settingService.LoadSettingAsync<DisplayTextSettings>(storeScope);

            displayTextSettings.DisplayText = model.TextToDisplay;

            await _settingService.SaveSettingOverridablePerStoreAsync(displayTextSettings, x => x.DisplayText, model.TextToDisplay_OverrideForStore
                , storeScope, false);

            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            return await Configure();
        }
    }
}

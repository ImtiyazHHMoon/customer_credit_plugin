﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.DisplayText.Components
{
    [ViewComponent(Name = "WidgetsDisplayText")]
    public class WidgetsDisplayTextViewComponent : NopViewComponent
    {
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        public WidgetsDisplayTextViewComponent(IStoreContext storeContext, ISettingService settingService)
        {
            _storeContext = storeContext;
            _settingService = settingService;   
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            var displayTextSettings = await _settingService.LoadSettingAsync<DisplayTextSettings>((await _storeContext.GetCurrentStoreAsync()).Id);

            return Content(displayTextSettings.DisplayText);
        }
    }
}

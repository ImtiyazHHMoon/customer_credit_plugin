﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Migrations
{
    [NopMigration("2021/11/23 15:11:16:2551770", "Product. Add some new property")]
    public class AddIsCampaignedProperty : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Column(nameof(Product.IsCampaigned))
            .OnTable(nameof(Product))
            .AsBoolean()
            .Nullable();
        }
    }
}
